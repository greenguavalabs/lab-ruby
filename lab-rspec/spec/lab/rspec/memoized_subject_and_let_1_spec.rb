describe Lab::RSpec::Froyo do
  subject(:froyo) { described_class.new }
  let(:toppings) { %w[sprinkles 'gummy bears'] }

  it "costs $4.50 without toppings" do
    expect(froyo.price).to eq(4.50)
  end

  it "costs $5.00 with 2 toppings" do
    froyo.add_toppings toppings
    expect(froyo.price).to eq(5.00)
  end
end
