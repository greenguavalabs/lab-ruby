describe Lab::RSpec::Froyo do
  subject(:froyo) { described_class.new }
  let(:toppings) { [] }

  before { froyo.add_toppings toppings }

  describe "#price" do
    shared_examples "real price" do
      it "costs at least $0" do
        expect(froyo.price).to be >= 0.0
      end
    end

    context "with no toppings" do
      it_behaves_like "real price"
    end

    context "with 2 toppings" do
      let(:toppings) { %w[sprinkles 'gummy bears'] }

      it_behaves_like "real price"
    end

    # more scenarios and tests
  end
end
