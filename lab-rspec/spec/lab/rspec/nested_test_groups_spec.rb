describe Lab::RSpec::Froyo do
  describe "#add_topping" do
    # add scenarios and test cases
  end

  describe "#price" do
    it "returns a number >= 0" do
      # some test code
    end

    context "with no toppings" do
      it "costs $4.5" do
        # some test code
      end
    end

    context "with 2 toppings" do
      it "costs $5.0" do
        # some test code
      end

      it "cost $4.5 with free toppings coupon" do
        # some test code
      end
    end

    # more contexts
  end
end
