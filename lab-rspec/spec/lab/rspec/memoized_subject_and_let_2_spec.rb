describe Lab::RSpec::Froyo do
  subject(:froyo) { described_class.new }
  let(:toppings) { [] }

  before { froyo.add_toppings toppings }

  it "costs $4.50 without toppings" do
    expect(froyo.price).to eq(4.50)
  end

  context "with 2 toppings" do
    let(:toppings) { %w[sprinkles 'gummy bears'] }

    it "costs $5.00 with 2 toppings" do
      expect(froyo.price).to eq(5.00)
    end
  end

  # more scenarios and tests
end
