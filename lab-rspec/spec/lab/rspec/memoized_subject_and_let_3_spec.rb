describe Lab::RSpec::Froyo do
  subject(:froyo) { described_class.new }
  let(:toppings) { [] }

  before { froyo.add_toppings toppings }

  describe "#price" do
    let(:coupon) { "FREE_TOPPINGS" }

    context "with no toppings" do
      it "costs $4.50" do
        expect(froyo.price).to eq(4.50)
      end

      it "costs $4.50 with a free toppings coupon" do
        expect(froyo.price(coupon)).to eq(4.50)
      end
    end

    context "with 2 toppings" do
      let(:toppings) { %w[sprinkles 'gummy bears'] }

      it "costs $5" do
        expect(froyo.price).to eq(5.00)
      end

      it "costs $4.50 with a free toppings coupon" do
        expect(froyo.price(coupon)).to eq(4.50)
      end
    end

    # more scenarios and tests
  end
end
