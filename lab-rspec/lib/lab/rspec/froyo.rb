module Lab
  module RSpec
    # Froyo is a sample class used in the rspec examples.
    class Froyo
      def initialize
        @toppings_count = 0
      end

      def add_toppings(toppings = nil)
        @toppings_count += toppings.size unless toppings.nil?
      end

      def price(coupon = nil)
        return 5.0 if @toppings_count > 0 && coupon.nil?
        4.5
      end
    end
  end
end
