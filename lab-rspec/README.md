# lab-rspec
Sample code demonstrating features of `rspec`.

Highlighted features:
* hierarchical `before` and `after`
* memoized `subject` and `let`
* nested example groups
* example group reuse with `shared_examples`
