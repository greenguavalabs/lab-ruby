SimpleCov.minimum_coverage 100
SimpleCov.start do
  add_filter "/spec/"
end
