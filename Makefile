DEVBOX_CONTAINER=lab_ruby_devbox

.DEFAULT:
	$(call CMD, bundle)
	$(call CMD, rake $@)

.PHONY: rake
rake:
	$(call CMD, bundle)
	$(call CMD, rake)

.PHONY: devenv
devenv:
	cd dev/docker && docker-compose up -d

-include dev/env.mk
