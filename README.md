# lab-ruby

A repository of source code that illustrates libraries, concepts and patterns useful to `ruby` developers.

Labs:
* [lab-rspec](/lab-rspec/README.md)
