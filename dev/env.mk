# generate the wrapper for the command to run
ifdef NO_DOCKER
	CMD = $(1)
else
	CMD = $(PTY) docker exec -t $(DEVBOX_CONTAINER) bash -c "cd /src && $(1)"
endif
